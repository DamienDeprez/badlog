/*******************************************************/ 
/**** LELEC_2811  4 sensors logger  by P. Gerard UCL ***/
/**** 23/11/2016                                     ***/
/**** Review by D. Damien 12/2017                    ***/
/*******************************************************/ 

#include "mbed.h"
#include "FreescaleIAP.h"   // Library for Flash Access
#include "MMA8491Q_PG.h"
#include "MAG3110.h"        // Magnetometer
#include "MPL3115A2.h"      // Library for Altimeter / Temperature Access
#include "tsi_sensor.h"     // For Slider : J10 side = START_ACQUISITION and J1 side = STOP_ACQUISITION

#define KL25Z_VDD 2.905     // !!! Value of VDD : To be measured on board KL25Z pin P3V3 (calibration)

#define MMA8491_I2C_ADDRESS (0x55<<1)
#define REG_OUT_X_MSB           0x01
#define REG_OUT_Y_MSB           0x03
#define REG_OUT_Z_MSB           0x05

#define MPL3115A2_I2C_ADDRESS (0x60<<1)
#define REG_ALTIMETER_MSB   0x01

#define DISABLE_STATE           0
#define ENABLE_STATE            1

#define MAG_ADDR 0x1D  

#define NO_JUMPER               0
#define JUMPER_PRESENT          1

#define LEVEL_0                 0
#define LEVEL_1                 1

#define LED_ON                  0
#define LED_OFF                 1

#define DISABLE_STATE           0
#define ENABLE_STATE            1

#define FLASH_NO_ACQ_DONE       0
#define FLASH_ACQ_DONE          1
#define ERASE_FLASH_ERROR       -1
#define WRITE_FLASH_ERROR       -2

#define SECTOR_SIZE             1024
#define RESERVED_SECTOR         32

#define ACQ_TIMER_PERIOD        0.05           // Time between 2 acquisitions (here 50 mSec or f=20 Hz)

//Structure of Sensors Datas !!!!! SIZE MUST BE A MULTIPLE OF 4 bytes !!!!!
typedef struct{
    int16_t Accel_X;                // 2 bytes
    int16_t Accel_Y;                // 2 bytes
    int16_t Accel_Z;                // 2 bytes
    int16_t Mag_X;                  // 2 bytes
    int16_t Mag_Y;                  // 2 bytes
    int16_t Mag_Z;                  // 2 bytes
    float Barometer;                // 4 bytes
    float Temperature;              // 4 bytes
    unsigned short Analog_PTE20;    // 2 bytes
    unsigned short Analog_PTE21;    // 2 bytes
    unsigned short Analog_PTE22;    // 2 bytes
    unsigned short Analog_PTE23;    // 2 bytes
} Sensor_Data;                      // TOTAL = 28 bytes 


// --- Setup I2C for MMA8491
MMA8491Q my8491(PTE0, PTE1, MMA8491_I2C_ADDRESS);
 
DigitalOut Accel_Enable(PTA13);
 
// --- Setup I2C for MAG3110
MAG3110 myMA3110(PTE0, PTE1);
 
// --- Set Sensors Barometer - Temperature
MPL3115A2 myMPL3115( PTE0, PTE1, MPL3115A2_I2C_ADDRESS);
 
// --- Set Serial Port
Serial Host_Comm(USBTX, USBRX); // tx, rxSerial pc(USBTX, USBRX); // tx, rx
 
TSIAnalogSlider tsi(9, 10, 40);             // Slider finger on 10%-30% -> START_ACQ 
                                            //        finger on 60%-80% -> STOP_ACQ  
 
AnalogIn myPTE20(PTE20);
AnalogIn myPTE21(PTE21);
AnalogIn myPTE22(PTE22);
AnalogIn myPTE23(PTE23);
 
Ticker myTick_Acq;              // Periodical timer for Acquisition
 
DigitalOut Led_Red(LED1);       // Define I/O for Leds
DigitalOut Led_Green(LED2);
DigitalOut Led_Blue(LED3);

DigitalOut Start_Pulse_Out(PTC9);   // Used to enter/exit Acquisition mode 
DigitalIn  Start_Pulse_In(PTC11);   // ShortPin J1_15 and J1_16 to enter in Acq_Mode

// Globale variable
volatile bool bTimer;               // 1 means a Timer tick is done

int Flash_Base_Address = RESERVED_SECTOR * SECTOR_SIZE ; // Store Flash Base Adresse with 32K reserved for Application Code
int Nb_Sector;
uint32_t KL25_Flash_Size;

bool write_in_flash = true; // false -> send data through comm port - true -> save data to the flash memory

// Function Declaration

void Clear_Led(void);
int Acquisition(void);
int Read_Data_Logging(void);
bool Check_Jumper(void);
void myTimer_Acq_Task(void);
void Acquisition_Task(void);
void Read_Task(void);

extern IAPCode verify_erased(int address, unsigned int length);

int main() {

    uint8_t Count;
        
    
    Accel_Enable = DISABLE_STATE;               // Turn Accel. Enable I/O to disabled state
 
// --- Baud rate setting
    Host_Comm.baud(115200);
 
    Clear_Led();
    
    KL25_Flash_Size = flash_size();            // Get Size of KL25 Embedded Flash
    Nb_Sector = (KL25_Flash_Size / SECTOR_SIZE) - RESERVED_SECTOR; // Reserve Max 32K for App Code
      
    myTick_Acq.attach(&myTimer_Acq_Task, ACQ_TIMER_PERIOD); // Initialize Timer Interrupt
  
    Host_Comm.printf("\n\rLELEC2811 4 sensors Logger V1.0 UCL 2016\n\r");
    
    if ((sizeof(Sensor_Data) % 4) != 0)
    {
        Host_Comm.printf("\n\rERROR! Acquisition Data Structure size is NOT a multiple of 4!!! (%d)", sizeof(Sensor_Data));
        for(;;);        
    }
 
    for (;;)
    {
        if (Check_Jumper() == JUMPER_PRESENT)        // Touch Slider J10 side ?               
        {
            Clear_Led();
            
            Count = 5;
            while (Count !=0)
            {
                if (Check_Jumper() == JUMPER_PRESENT)   
                {
                    Led_Blue = LED_ON;
                    Led_Green = LED_ON;
                    Led_Red = LED_ON;     // Blink to alert user "Enter in Acquisition" after 5 seconds
                    wait_ms(900);
                    Led_Blue = LED_OFF;
                    Led_Green = LED_OFF;
                    Led_Red = LED_OFF;
                    wait_ms(100);
                    Count --;
                  if (Count == 0)
                    {        
                        Acquisition_Task();
                    }
                }
                else
                {
                    Count = 0;
                }
            } 
        }
        else
        {
            Read_Task();         
        }
    }
}
    
void Read_Task(){
    char host_cmd;
    IAPCode Flash_State;
    bool bAcq_Done;
    
    Flash_State = verify_erased(Flash_Base_Address, KL25_Flash_Size - (RESERVED_SECTOR * SECTOR_SIZE)); 
    if (Flash_State == 0) // Virgin Flash ?
    {
        bAcq_Done = 0;      
    }
    else
    {
        bAcq_Done = 1;
    }     
    
    Led_Blue = LED_OFF;
    //Clear_Led();
    //wait_ms(500);
                
    if (bAcq_Done == 1)
    {
        Led_Red = LED_OFF;
        Led_Green = LED_ON;
    }
    else
    {
        Led_Green = LED_OFF;
        Led_Red = LED_ON;
    }
           
    if(Host_Comm.readable())                    // Did we receive a char from Host ?
    {
        host_cmd = Host_Comm.getc();            // Get it
        
        if((host_cmd == 'M')){ // Set the mode to write in flash memory ?
              write_in_flash = true;
        }
        if(host_cmd == 'm'){ // Set the mode to transmit directly ?
            write_in_flash = false;
        }
                  
        if ((host_cmd == 'R') || (host_cmd == 'r')) // Read Flash Command ?
        {    
            Read_Data_Logging();                // Read and send acquisition data                   
        }
    }
    wait_ms(50);          
}

void Acquisition_Task()
{
    int Acq_Status;
               
    Clear_Led();    
    Acq_Status = Acquisition();
    Clear_Led();
    
    while (Check_Jumper() == JUMPER_PRESENT)    // Touche Slider J1 side ?
    {   
        if (Acq_Status != FLASH_ACQ_DONE)
        {
            Led_Red = !Led_Red;
        }
        else
        {
           Led_Green = !Led_Green;            
        }
        wait_ms(100);
    }
}

void Clear_Led(void)
{
    Led_Red = LED_OFF;
    Led_Green = LED_OFF;
    Led_Blue = LED_OFF ;     // Bug on board : Turning On Blue Led decrease consumption...
}

bool Check_Jumper()         // If J1_15 and J1_16 connected together -> return JUMPER_PRESENT
{
    uint8_t i;
    
    for (i = 0 ; i < 2 ; i ++)
    {
        Start_Pulse_Out = LEVEL_1;
        wait_ms(1);
        if (Start_Pulse_In != LEVEL_1)
        {
            return NO_JUMPER;
        }
    
        Start_Pulse_Out = LEVEL_0;
        wait_ms(1);
        if (Start_Pulse_In != LEVEL_0)
        {
            return NO_JUMPER;
        }
    }
    return JUMPER_PRESENT;
}

int Acquisition(void)
{
    int Status;  
    int Flash_Ptr ;
    int Led_Counter;
    int Record_Counter = 0;
    
    float Voltage_PTE20;
    float Voltage_PTE21;
    float Voltage_PTE22;
    float Voltage_PTE23;
    
    Sensor_Data myData;  
    
/*** Erase all Flash Page if we are in write mode **/          
    if(write_in_flash){
        for (Flash_Ptr = Flash_Base_Address ; Flash_Ptr < KL25_Flash_Size ; Flash_Ptr += 0x400)
        {
            Status = erase_sector(Flash_Ptr);   // Erase sector
                     
            if (Status !=0)
            { 
                return ERASE_FLASH_ERROR;
            }    
        }
    }
  
    Flash_Ptr = Flash_Base_Address; // Begin of Storage Area in Flash
    
    Led_Green = LED_ON;
    
    Led_Counter = 0;

    //Slider_Off_Count = 0;
    
/***** Begin of Loop Acquisition - Write in Flash ***/  

    while (Flash_Ptr < (KL25_Flash_Size - sizeof(Sensor_Data)) )            // Acq Loop
    {              
        while (bTimer == 0)                         // Wait Acq Tick Timer Done
        {
  
        }
        bTimer = 0;
                   
        if ((float) Led_Counter * ACQ_TIMER_PERIOD == 0.5) // Blink at 2 Hz
        { 
            Led_Counter = 0;
            Led_Green = !Led_Green;
        }
        
        Led_Counter++; 
        

        // Get Accelerometer datas        
        Accel_Enable = ENABLE_STATE;                    // Rising Edge -> Start Accel Measure
  
        myData.Accel_X = my8491.getAccAxis(REG_OUT_X_MSB);
        myData.Accel_Y = my8491.getAccAxis(REG_OUT_Y_MSB);
        myData.Accel_Z = my8491.getAccAxis(REG_OUT_Z_MSB);       
        
        Accel_Enable = DISABLE_STATE;
 
        // Get Barometer Value
        myData.Barometer = myMPL3115.getPressure();
        // Get Temperature data       
        myData.Temperature = myMPL3115.getTemperature();
       
        // Get Magnetometer datas            
        myData.Mag_X = myMA3110.readVal(MAG_OUT_X_MSB);
        myData.Mag_Y = myMA3110.readVal(MAG_OUT_Y_MSB);
        myData.Mag_Z = myMA3110.readVal(MAG_OUT_Z_MSB);      

        // Get Analog Value
        myData.Analog_PTE20 = myPTE20.read_u16(); 
        myData.Analog_PTE21 = myPTE21.read_u16();  
        myData.Analog_PTE22 = myPTE22.read_u16();  
        myData.Analog_PTE23 = myPTE23.read_u16();
           
            
        /*** Save Data in Flash if we are in write mode else send data directly ***/   
        if(write_in_flash){
            Status = program_flash(Flash_Ptr, (char *) &myData, sizeof(Sensor_Data));    // Write in the Flash
            if (Status != 0) 
            {
                 Host_Comm.printf("\n\rFlash_Write Error = %d", Status); 
                 return WRITE_FLASH_ERROR;
            }
            Flash_Ptr += sizeof(Sensor_Data); 
        }
        else{
            Voltage_PTE20 = ((float) myData.Analog_PTE20 / 0XFFFF) * KL25Z_VDD ;          // Convert in Voltage
            Voltage_PTE21 = ((float) myData.Analog_PTE21 / 0XFFFF) * KL25Z_VDD ;          
            Voltage_PTE22 = ((float) myData.Analog_PTE22 / 0XFFFF) * KL25Z_VDD ;          
            Voltage_PTE23 = ((float) myData.Analog_PTE23 / 0XFFFF) * KL25Z_VDD ;
            // Data are encoded using the following format : S0...Data...0S with S0 and 0S the boundaries for detection of a frame
            Host_Comm.printf("\n\rS0 %d",Record_Counter);
            Host_Comm.printf("%d %d %d ", myData.Accel_X, myData.Accel_Y, myData.Accel_Z);
            Host_Comm.printf("%1.2f ", myData.Barometer);
            Host_Comm.printf("%1.2f ", myData.Temperature);
            Host_Comm.printf("%d %d %d ", myData.Mag_X, myData.Mag_Y, myData.Mag_Z);
            Host_Comm.printf("%1.3f %1.3f %1.3f %1.3f ", Voltage_PTE20, Voltage_PTE21, Voltage_PTE22, Voltage_PTE23);
            Record_Counter++;
        }
        if (Check_Jumper() != JUMPER_PRESENT)       // Touch Slider at J10 side to stop Acquisition?
        {
            if(!write_in_flash){ // send the end boundaries
                Host_Comm.printf("$");
            }
            return FLASH_ACQ_DONE ;             // YES so Stop Acquisition
        }        
    }
    return FLASH_ACQ_DONE ;
}

int Read_Data_Logging()
{  
    Sensor_Data * data = (Sensor_Data * )Flash_Base_Address; // Sensor_Data pointer of data stored in Flash
    int Flash_Record_Ptr;
    char cmd;
    int Record_Counter;
    int Max_Record;
    
    float Voltage_PTE20;
    float Voltage_PTE21;
    float Voltage_PTE22;
    float Voltage_PTE23;
    
    Sensor_Data myRead_Data;            // Data Structure used to retrieve saved value from Flash
    
    Clear_Led(); 
  
    Max_Record = (Nb_Sector * SECTOR_SIZE) / sizeof(Sensor_Data);
    Record_Counter = 0;
    Flash_Record_Ptr = 0;
     
    while (Record_Counter < Max_Record) 
    {         
        Led_Green = !Led_Green;
        Led_Blue = !Led_Green;
        
        if(Host_Comm.readable()) 
        {
            cmd = Host_Comm.getc();
            if ((cmd == 'S') || (cmd == 's'))           // Receiving 'S' or 's' means stop Read Flash
            {
                Clear_Led(); 
                return 0;    
            }
        }
        
        myRead_Data = data[Flash_Record_Ptr];
        
        Flash_Record_Ptr ++;
                       
        Voltage_PTE20 = ((float) myRead_Data.Analog_PTE20 / 0XFFFF) * KL25Z_VDD ;          // Convert in Voltage
        Voltage_PTE21 = ((float) myRead_Data.Analog_PTE21 / 0XFFFF) * KL25Z_VDD ;          
        Voltage_PTE22 = ((float) myRead_Data.Analog_PTE22 / 0XFFFF) * KL25Z_VDD ;          
        Voltage_PTE23 = ((float) myRead_Data.Analog_PTE23 / 0XFFFF) * KL25Z_VDD ; 
        Host_Comm.printf("\n\rS0 %d",Record_Counter);
        Host_Comm.printf("%d %d %d ", myRead_Data.Accel_X, myRead_Data.Accel_Y, myRead_Data.Accel_Z);
        Host_Comm.printf("%1.2f ", myRead_Data.Barometer);
        Host_Comm.printf("%1.2f ", myRead_Data.Temperature);
        Host_Comm.printf("%d %d %d ", myRead_Data.Mag_X, myRead_Data.Mag_Y, myRead_Data.Mag_Z);
        Host_Comm.printf("%1.3f %1.3f %1.3f %1.3f ", Voltage_PTE20, Voltage_PTE21, Voltage_PTE22, Voltage_PTE23);
        Record_Counter++;
    }
    Clear_Led();
    Host_Comm.printf("$");
    return 0;
}

/* Interrupt Task */
void myTimer_Acq_Task() 
{   
    bTimer = 1;
}

