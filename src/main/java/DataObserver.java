import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

import java.io.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class DataObserver {
    private Hashtable<Integer,Data> set = new Hashtable<>();
    //private ObservableList<Data> data = FXCollections.emptyObservableList();
    StringBuilder text = new StringBuilder("How to use the software : \n" +
            "\t1) Before launching the application, connect the Platform using the usb key (on Unix system you need to give the read access : \"sudo chmod a+rw /dev/...\")\n" +
            "\t2) Configure the mode (store mode / direct acq mode)\n" +
            "\t3) If you use the direct acq mode, start the capture on the software and after on the platform\n" +
            "\t4) If you use the store mode, start the reading when you have data on the platform\n" +
            "\n" +
            "\n" +
            "╔═════════════════╦═════════════════════════════╦═════════════════════════════╦════════════════╦════════════════════╦═══════════════════════════════════════════╗\n" +
            "║                 ║      Accelerometer (mG)     ║         Magnetometer        ║                ║                    ║                Voltage (V)                ║\n" +
            "║  Record Number  ╟─────────┬─────────┬─────────╫─────────┬─────────┬─────────╢  Presure (Pa)  ║  Temperature (°C)  ╟──────────┬──────────┬──────────┬──────────╢\n" +
            "║                 ║  Axe X  │  Axe Y  │  Axe Z  ║  Axe X  │  Axe Y  │  Axe Z  ║                ║                    ║   V-20   │   V-21   │   V-22   │   V-23   ║\n" +
            "╠═════════════════╬═════════╪═════════╪═════════╬═════════╪═════════╪═════════╬════════════════╬════════════════════╬══════════╪══════════╪══════════╪══════════╣\n");
    private int length = 0;

    public List<Data> getData() {
        ArrayList<Data> list = new ArrayList<>(set.values());
        Collections.sort(list);
        return list;
    }

    public Data getOneData(int pos) {return  set.get(pos) ;}

    public void setData(List<Data> data){
        for (Data d : data){
            text.append(d);
            set.put(d.getRecord_number(),d);
        }
        this.length = data.size();
    }

    public int getLength() {
        return length;
    }

    public void add(Data data){
        this.text.append(data);
        this.set.put(data.getRecord_number(),data);
        length++;
    }

    public String getText(){
        return text.toString();
    }

    public void removeAll(){
        text.delete(0,text.length());
        set.clear();
        length = 0;
    }

    public void data2csv(File file) throws IOException{
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        for (Data d : set.values()){
            String line = d.getRecord_number()+","
                    + d.getAccelerometer_x()+","
                    + d.getAccelerometer_y()+","
                    + d.getAccelerometer_z()+","
                    + d.getMagnetometer_x()+","
                    + d.getMagnetometer_y()+","
                    + d.getMagnetometer_z()+","
                    + d.getTemperature()+","
                    + d.getPressure()+","
                    + d.getVoltage_20()+","
                    + d.getVoltage_21()+","
                    + d.getVoltage_22()+","
                    + d.getVoltage_23()+"\n";
            writer.write(line);
        }
        writer.flush();
        writer.close();
    }
}
