/**
 * Created by damien on 7/11/17.
 */
public enum Mode {
    COMM("No memory"),
    READ("Memory");

    private String name = "";

    Mode (String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
