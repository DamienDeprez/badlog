import com.fazecast.jSerialComm.SerialPort;
//import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import eu.hansolo.enzo.led.Led;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javafx.util.StringConverter;

public class Controller {

    private XYChart.Series<Integer, Integer> acceleromter_x_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Integer> acceleromter_y_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Integer> acceleromter_z_series = new XYChart.Series<>();

    private XYChart.Series<Integer, Integer> magnometer_x_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Integer> magnometer_y_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Integer> magnometer_z_series = new XYChart.Series<>();

    private XYChart.Series<Integer, Float> perssure_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> temp_series = new XYChart.Series<>();

    private XYChart.Series<Integer, Float> volt_20_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> volt_21_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> volt_22_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> volt_23_series = new XYChart.Series<>();

    private XYChart.Series<Integer, Float> acceleration_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> pithc_angle_series = new XYChart.Series<>();
    private XYChart.Series<Integer, Float> roll_angle_series = new XYChart.Series<>();

    private Timeline update_data;
    private Timeline update_port;
    private int position;
    private boolean running = false;

    private static final int frequency = 400;
    private static final int time_resolution = 30*frequency/20;
    private static final int time_tick = 5*frequency/20;

    double orgY, orgTranslateY;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private MenuItem menu_open;

    @FXML
    private MenuItem menu_close;

    @FXML
    private MenuItem menu_save;

    @FXML
    private MenuItem menu_sava_as;

    @FXML
    private MenuItem menu_config;

    @FXML
    private MenuItem menu_exit;

    @FXML
    private ChoiceBox<String> port_list;

    @FXML
    private ChoiceBox<Integer> rate_list;

    @FXML
    private ChoiceBox<Mode> mode_list;

    @FXML
    private Button start;

    @FXML
    private Led led;

    @FXML
    private TextArea raw_data_text;

    @FXML
    private LineChart<Integer, Integer> acc_x;

    @FXML
    private LineChart<Integer, Integer> acc_y;

    @FXML
    private LineChart<Integer, Integer> acc_z;

    @FXML
    private LineChart<Integer, Integer> mag_x;

    @FXML
    private LineChart<Integer, Integer> mag_y;

    @FXML
    private LineChart<Integer, Integer> mag_z;

    @FXML
    private LineChart<Integer, Float> pressure;

    @FXML
    private LineChart<Integer, Float> temperature;

    @FXML
    private LineChart<Integer, Float> volt_1;

    @FXML
    private LineChart<Integer, Float> volt_2;

    @FXML
    private LineChart<Integer, Float> volt_3;

    @FXML
    private LineChart<Integer, Float> volt_4;

    @FXML
    private NumberAxis acc_tx;

    @FXML
    private NumberAxis acc_ty;

    @FXML
    private NumberAxis acc_tz;

    @FXML
    private NumberAxis mag_tx;

    @FXML
    private NumberAxis mag_ty;

    @FXML
    private NumberAxis mag_tz;

    @FXML
    private NumberAxis alt_t1;

    @FXML
    private NumberAxis alt_t2;

    @FXML
    private NumberAxis volt_t1;

    @FXML
    private NumberAxis volt_t2;

    @FXML
    private NumberAxis volt_t3;

    @FXML
    private NumberAxis volt_t4;

    @FXML
    void initialize() {

        //<editor-fold defaultstate="collapsed" desc="Injection from FXML">
        assert port_list != null : "fx:id=\"port_list\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert rate_list != null : "fx:id=\"rate_list\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mode_list != null : "fx:id=\"mode_list\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert start != null : "fx:id=\"start\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert led != null : "fx:id=\"led\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_x != null : "fx:id=\"acc_x\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_tx != null : "fx:id=\"acc_tx\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_y != null : "fx:id=\"acc_y\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_ty != null : "fx:id=\"acc_ty\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_z != null : "fx:id=\"acc_z\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert acc_tz != null : "fx:id=\"acc_tz\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_x != null : "fx:id=\"mag_x\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_tx != null : "fx:id=\"mag_tx\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_y != null : "fx:id=\"mag_y\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_ty != null : "fx:id=\"mag_ty\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_z != null : "fx:id=\"mag_z\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert mag_tz != null : "fx:id=\"mag_tz\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert pressure != null : "fx:id=\"pressure\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert alt_t1 != null : "fx:id=\"alt_t1\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert temperature != null : "fx:id=\"temperature\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert alt_t2 != null : "fx:id=\"alt_t2\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_1 != null : "fx:id=\"volt_1\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_t1 != null : "fx:id=\"volt_t1\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_2 != null : "fx:id=\"volt_2\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_t2 != null : "fx:id=\"volt_t2\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_3 != null : "fx:id=\"volt_3\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_t3 != null : "fx:id=\"volt_t3\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_4 != null : "fx:id=\"volt_4\" was not injected: check your FXML file 'BadLog.fxml'.";
        assert volt_t4 != null : "fx:id=\"volt_t4\" was not injected: check your FXML file 'BadLog.fxml'.";
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Initialize graph">
        TimeFormatter formatter = new TimeFormatter();

        acc_tx.setAutoRanging(false);
        acc_tx.setTickLabelFormatter(formatter);
        acc_tx.setTickUnit(time_tick); // tick 20 s
        acc_tx.setUpperBound(time_resolution); // 1 minutes

        acc_ty.setAutoRanging(false);
        acc_ty.setTickLabelFormatter(formatter);
        acc_ty.setTickUnit(time_tick); // tick 20 s
        acc_ty.setUpperBound(time_resolution); // 1 minutes

        acc_tz.setAutoRanging(false);
        acc_tz.setTickLabelFormatter(formatter);
        acc_tz.setTickUnit(time_tick); // tick 20 s
        acc_tz.setUpperBound(time_resolution); // 1 minutes

        mag_tx.setAutoRanging(false);
        mag_tx.setTickLabelFormatter(formatter);
        mag_tx.setTickUnit(time_tick); // tick 20 s
        mag_tx.setUpperBound(time_resolution); // 1 minutes

        mag_ty.setAutoRanging(false);
        mag_ty.setTickLabelFormatter(formatter);
        mag_ty.setTickUnit(time_tick); // tick 20 s
        mag_ty.setUpperBound(time_resolution); // 1 minutes

        mag_tz.setAutoRanging(false);
        mag_tz.setTickLabelFormatter(formatter);
        mag_tz.setTickUnit(time_tick); // tick 20 s
        mag_tz.setUpperBound(time_resolution); // 1 minutes

        alt_t1.setAutoRanging(false);
        alt_t1.setTickLabelFormatter(formatter);
        alt_t1.setTickUnit(time_tick); // tick 20 s
        alt_t1.setUpperBound(time_resolution); // 1 minutes

        alt_t2.setAutoRanging(false);
        alt_t2.setTickLabelFormatter(formatter);
        alt_t2.setTickUnit(time_tick); // tick 20 s
        alt_t2.setUpperBound(time_resolution); // 1 minutes

        volt_t1.setAutoRanging(false);
        volt_t1.setTickLabelFormatter(formatter);
        volt_t1.setTickUnit(time_tick); // tick 20 s
        volt_t1.setUpperBound(time_resolution); // 1 minutes

        volt_t2.setAutoRanging(false);
        volt_t2.setTickLabelFormatter(formatter);
        volt_t2.setTickUnit(time_tick); // tick 20 s
        volt_t2.setUpperBound(time_resolution); // 1 minutes

        volt_t3.setAutoRanging(false);
        volt_t3.setTickLabelFormatter(formatter);
        volt_t3.setTickUnit(time_tick); // tick 20 s
        volt_t3.setUpperBound(time_resolution); // 1 minutes

        volt_t4.setAutoRanging(false);
        volt_t4.setTickLabelFormatter(formatter);
        volt_t4.setTickUnit(time_tick); // tick 20 s
        volt_t4.setUpperBound(time_resolution); // 1 minutes

        acc_x.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        acc_y.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        acc_z.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        mag_x.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        mag_y.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        mag_z.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        pressure.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        temperature.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        volt_1.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        volt_2.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        volt_3.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        volt_4.setOnScroll( (ScrollEvent e) ->{
            double p = (e.getX()-acc_x.getYAxis().getWidth())/acc_tx.getWidth();
            double t = acc_tx.getLowerBound();
            double w = acc_tx.getUpperBound()-t;
            zoom(e,p,t,w);
        });

        //</editor-fold>

        /**/
        String [] ports = App.comm.getPorts();

        port_list.getItems().addAll(ports);
        if(ports.length > 0)
            port_list.setValue(ports[0]);

        mode_list.getItems().add(Mode.COMM);
        mode_list.getItems().add(Mode.READ);

        rate_list.getItems().addAll(300, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200, 230400);
        rate_list.setValue(115200);

        mode_list.setValue(Mode.COMM);

        menu_open.setOnAction(ActionEvent->{
            reset();
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open a file");
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("BadLog raw data (*.bdlrw)", "*.bdlrw");
            chooser.getExtensionFilters().add(filter);
            App.save_file = chooser.showOpenDialog(null);
            if(App.save_file != null){
                App.primaryStage.setTitle("BadLog Data Analyser - "+App.save_file.getName());
                System.out.println("open file : " + App.save_file.getAbsolutePath());
                try{
                    InputStream in = Files.newInputStream(App.save_file.toPath());
                    ObjectInputStream ois = new ObjectInputStream(in);
                    List<Data> list = (List<Data>) ois.readObject();
                    for (Data d : list) App.mainLog.trace(d);
                    App.dataObserver.setData(list);
                }
                catch (IOException | ClassNotFoundException e){
                    e.printStackTrace();
                }
            }
        });

        menu_sava_as.setOnAction(ActionEvent ->{
            FileChooser.ExtensionFilter raw_data = new FileChooser.ExtensionFilter("BadLog raw data (*.bdlrw)", "*.bdlrw");
            FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("Comma-Separated Values (*.csv)", "*.csv");
            FileChooser.ExtensionFilter ext;
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Save file as");
            chooser.getExtensionFilters().add(raw_data);
            chooser.getExtensionFilters().add(csv);
            App.save_file = chooser.showSaveDialog(null);
            ext = chooser.getSelectedExtensionFilter();

            if(App.save_file != null)
            {
                try{
                    App.save_file.createNewFile();
                    OutputStream fos = Files.newOutputStream(App.save_file.toPath());
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    if(ext.equals(raw_data)) {
                        oos.writeObject(App.dataObserver.getData());
                        oos.close();
                    }
                    if(ext.equals(csv)){
                        App.dataObserver.data2csv(App.save_file);
                    }
                }
                catch (IOException e){
                    e.printStackTrace();
                }
                System.out.println("save file as : "+ App.save_file.getAbsolutePath());
            }
        });

        menu_save.setOnAction(ActionEvent->{
            FileChooser.ExtensionFilter raw_data = new FileChooser.ExtensionFilter("BadLog raw data (*.bdlrw)", "*.bdlrw");
            FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("Comma-Separated Values (*.csv)", "*.csv");
            FileChooser.ExtensionFilter ext;
            if(App.save_file == null){
                FileChooser chooser = new FileChooser();
                chooser.setTitle("Save file");
                chooser.getExtensionFilters().add(raw_data);
                //chooser.getExtensionFilters().add(csv);
                App.save_file = chooser.showSaveDialog(null);
                ext = chooser.getSelectedExtensionFilter();
            }
            else{
                ext = raw_data;
            }
            if(App.save_file != null)
            {
                try{
                    App.save_file.createNewFile();
                    OutputStream fos = Files.newOutputStream(App.save_file.toPath());
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    if(ext.equals(raw_data)) {
                        oos.writeObject(App.dataObserver.getData());
                        for(Data d : App.dataObserver.getData()) App.mainLog.debug(d);
                        oos.close();
                    }
                    if(ext.equals(csv)){
                        App.dataObserver.data2csv(App.save_file);
                    }
                }
                catch (IOException e){
                    e.printStackTrace();
                }
                System.out.println("save file : "+ App.save_file.getAbsolutePath());
            }
        });

        start.setText(App.resources.getString("gui.transfer.config.start.comm"));

        mode_list.setOnAction(ActionEvent->{
            switch (mode_list.getValue()){
                case COMM:
                    start.setText(App.resources.getString("gui.transfer.config.start.comm"));
                    led.setLedColor(Color.RED);
                    break;
                case READ:
                    start.setText(App.resources.getString("gui.transfer.config.start.read"));
                    led.setLedColor(Color.GREEN);
                    break;
            }
        });

        mode_list.setOnAction(EventHandler->{
            App.mainLog.debug("Change of the mode list : "+mode_list.getValue());
            App.comm.openPort(port_list.getValue());
            switch (mode_list.getValue()){
                case COMM:
                    App.comm.writePort("m");
                    break;
                case READ:
                    App.comm.writePort("M");
                    break;
            }
            App.comm.closePort();
        });

        rate_list.setOnAction(EventHandler -> App.comm.setBaudRate(rate_list.getValue()));

        start.setOnAction(EventHandler -> {
            reset();
            App.comm.openPort(port_list.getValue());
            switch (mode_list.getValue()){
                case COMM:
                    //App.comm.writePort("m");
                    App.comm.readPort();
                    break;
                case READ:
                    //App.comm.writePort("M");
                    App.comm.readPort();
                    App.comm.writePort("r");
                    break;
                default:
                    System.err.println("error - get mode");
                    break;
            }
        });

        menu_exit.setOnAction(EventHandler-> Platform.exit());

        acc_x.getData().add(acceleromter_x_series);
        acc_x.setLegendVisible(false);
        acc_y.getData().add(acceleromter_y_series);
        acc_y.setLegendVisible(false);
        acc_z.getData().add(acceleromter_z_series);
        acc_z.setLegendVisible(false);

        mag_x.getData().add(magnometer_x_series);
        mag_y.getData().add(magnometer_y_series);
        mag_z.getData().add(magnometer_z_series);

        pressure.getData().add(perssure_series);
        temperature.getData().add(temp_series);

        volt_1.getData().add(volt_20_series);
        volt_2.getData().add(volt_21_series);
        volt_3.getData().add(volt_22_series);
        volt_4.getData().add(volt_23_series);

        mag_x.setLegendVisible(false);
        mag_y.setLegendVisible(false);
        mag_z.setLegendVisible(false);

        pressure.setLegendVisible(false);
        temperature.setLegendVisible(false);

        volt_1.setLegendVisible(false);
        volt_2.setLegendVisible(false);
        volt_3.setLegendVisible(false);
        volt_4.setLegendVisible(false);

        update_data = new Timeline();
        update_data.getKeyFrames().add(new KeyFrame(Duration.millis(1), ActionEvent -> update()));
        update_data.setCycleCount(Animation.INDEFINITE);
        update_data.play();

        update_port = new Timeline();
        update_port.getKeyFrames().add(new KeyFrame(Duration.seconds(1),ActionEvent ->update_port_list()));
        update_port.setCycleCount(Animation.INDEFINITE);
        update_port.play();
    }

    public void reset(){
        raw_data_text.setText("How to use the software : \n" +
                "\t1) Before launching the application, connect the Platform using the usb key (on Unix system you need to give the read access : \"sudo chmod a+rw /dev/...\")\n" +
                "\t2) Configure the mode (store mode / direct acq mode)\n" +
                "\t3) If you use the direct acq mode, start the capture on the software and after on the platform\n" +
                "\t4) If you use the store mode, start the reading when you have data on the platform\n" +
                "\n" +
                "\n" +
                "╔═════════════════╦═════════════════════════════╦═════════════════════════════╦════════════════╦════════════════════╦═══════════════════════════════════════════╗\n" +
                "║                 ║      Accelerometer (mG)     ║         Magnetometer        ║                ║                    ║                Voltage (V)                ║\n" +
                "║  Record Number  ╟─────────┬─────────┬─────────╫─────────┬─────────┬─────────╢  Presure (Pa)  ║  Temperature (°C)  ╟──────────┬──────────┬──────────┬──────────╢\n" +
                "║                 ║  Axe X  │  Axe Y  │  Axe Z  ║  Axe X  │  Axe Y  │  Axe Z  ║                ║                    ║   V-20   │   V-21   │   V-22   │   V-23   ║\n" +
                "╠═════════════════╬═════════╪═════════╪═════════╬═════════╪═════════╪═════════╬════════════════╬════════════════════╬══════════╪══════════╪══════════╪══════════╣");
        position = 0;
        int size = acceleromter_x_series.getData().size();
        acceleromter_x_series.getData().remove(0,size);
        acceleromter_y_series.getData().remove(0,size);
        acceleromter_z_series.getData().remove(0,size);

        magnometer_x_series.getData().remove(0,size);
        magnometer_y_series.getData().remove(0,size);
        magnometer_z_series.getData().remove(0,size);

        perssure_series.getData().remove(0,size);
        temp_series.getData().remove(0,size);

        volt_20_series.getData().remove(0,size);
        volt_21_series.getData().remove(0,size);
        volt_22_series.getData().remove(0,size);
        volt_23_series.getData().remove(0,size);

        acceleration_series.getData().remove(0, size);
        pithc_angle_series.getData().remove(0, size);
        roll_angle_series.getData().remove(0, size);
        App.dataObserver.removeAll();
    }

    public void update_port_list(){
        String [] ports = App.comm.getPorts();
        for (String port : ports){
            if (!port_list.getItems().contains(port)){
                port_list.getItems().add(port);
            }
        }
    }

    public void update() {
        if(App.comm.isRunning())
        {
            led.setOn(true);
            start.setDisable(true);
            rate_list.setDisable(true);
            mode_list.setDisable(true);
            port_list.setDisable(true);
        }
        else{
            led.setOn(false);
            start.setDisable(false);
            rate_list.setDisable(false);
            mode_list.setDisable(false);
            port_list.setDisable(false);
        }
        if (position < App.dataObserver.getLength()) {
            Data data = App.dataObserver.getOneData(position);
            int record_number = data.getRecord_number();
            raw_data_text.setText(App.dataObserver.getText());
            raw_data_text.setScrollTop(Double.MAX_VALUE);
            acceleromter_x_series.getData().add(new XYChart.Data<>(record_number,data.getAccelerometer_x()));
            acceleromter_y_series.getData().add(new XYChart.Data<>(record_number,data.getAccelerometer_y()));
            acceleromter_z_series.getData().add(new XYChart.Data<>(record_number,data.getAccelerometer_z()));

            magnometer_x_series.getData().add(new XYChart.Data<>(record_number,data.getMagnetometer_x()));
            magnometer_y_series.getData().add(new XYChart.Data<>(record_number,data.getMagnetometer_y()));
            magnometer_z_series.getData().add(new XYChart.Data<>(record_number,data.getMagnetometer_z()));

            perssure_series.getData().add(new XYChart.Data<>(record_number,data.getPressure()));
            temp_series.getData().add(new XYChart.Data<>(record_number,data.getTemperature()));

            volt_20_series.getData().add(new XYChart.Data<>(record_number,data.getVoltage_20()));
            volt_21_series.getData().add(new XYChart.Data<>(record_number,data.getVoltage_21()));
            volt_22_series.getData().add(new XYChart.Data<>(record_number,data.getVoltage_22()));
            volt_23_series.getData().add(new XYChart.Data<>(record_number,data.getVoltage_23()));

            position++;
//            if(position > time_resolution+1 ){
//                acceleromter_x_series.getData().remove(0);
//                acceleromter_x_series.getData().remove(0);
//                acceleromter_x_series.getData().remove(0);
//            }

            if(position > time_resolution){
                acc_tx.setLowerBound(acc_tx.getLowerBound()+1);
                acc_tx.setUpperBound(acc_tx.getUpperBound()+1); // 1 minutes

                acc_ty.setLowerBound(acc_ty.getLowerBound()+1);
                acc_ty.setUpperBound(acc_ty.getUpperBound()+1); // 1 minutes

                acc_tz.setLowerBound(acc_tz.getLowerBound()+1);
                acc_tz.setUpperBound(acc_tz.getUpperBound()+1); // 1 minutes

                mag_tx.setLowerBound(mag_tx.getLowerBound() + 1);
                mag_tx.setUpperBound(mag_tx.getUpperBound() + 1);

                mag_ty.setLowerBound(mag_ty.getLowerBound() + 1);
                mag_ty.setUpperBound(mag_ty.getUpperBound() + 1);

                mag_tz.setLowerBound(mag_tz.getLowerBound() + 1);
                mag_tz.setUpperBound(mag_tz.getUpperBound() + 1);

                alt_t1.setLowerBound(alt_t1.getLowerBound() + 1);
                alt_t1.setUpperBound(alt_t1.getUpperBound() + 1);

                alt_t2.setLowerBound(alt_t2.getLowerBound() + 1);
                alt_t2.setUpperBound(alt_t2.getUpperBound() + 1);

                volt_t1.setLowerBound(volt_t1.getLowerBound() + 1);
                volt_t1.setUpperBound(volt_t1.getUpperBound() + 1);

                volt_t2.setLowerBound(volt_t2.getLowerBound() + 1);
                volt_t2.setUpperBound(volt_t2.getUpperBound() + 1);

                volt_t3.setLowerBound(volt_t3.getLowerBound() + 1);
                volt_t3.setUpperBound(volt_t3.getUpperBound() + 1);

                volt_t4.setLowerBound(volt_t4.getLowerBound() + 1);
                volt_t4.setUpperBound(volt_t4.getUpperBound() + 1);

            }
        }

    }

    private void zoom(ScrollEvent event, double p ,double t, double w){
        /*double p = (event.getX()-width)/chart_width;
        double t = acc_tx.getLowerBound();
        double w = acc_tx.getUpperBound()-t;*/
        double new_width;
        double m = event.getDeltaY()>0 ? 0.25 : 1.0;
        /*if(event.getDeltaY()>0)
            m = 1.0;//0.5/2.0;
        else
            m = 2.0/2.0;*/
        if((t+(p-m)*w)>0) {
            acc_tx.setLowerBound(t + (p - m) * w);
            acc_ty.setLowerBound(t + (p - m) * w);
            acc_tz.setLowerBound(t + (p - m) * w);
            mag_tx.setLowerBound(t + (p - m) * w);
            mag_ty.setLowerBound(t + (p - m) * w);
            mag_tz.setLowerBound(t + (p - m) * w);
            alt_t1.setLowerBound(t + (p - m) * w);
            alt_t2.setLowerBound(t + (p - m) * w);
            volt_t1.setLowerBound(t + (p - m) * w);
            volt_t2.setLowerBound(t + (p - m) * w);
            volt_t3.setLowerBound(t + (p - m) * w);
            volt_t4.setLowerBound(t + (p - m) * w);
        }
        else{
            acc_tx.setLowerBound(0);
            acc_ty.setLowerBound(0);
            acc_tz.setLowerBound(0);
            mag_tx.setLowerBound(0);
            mag_ty.setLowerBound(0);
            mag_tz.setLowerBound(0);
            alt_t1.setLowerBound(0);
            alt_t2.setLowerBound(0);
            volt_t1.setLowerBound(0);
            volt_t2.setLowerBound(0);
            volt_t3.setLowerBound(0);
            volt_t4.setLowerBound(0);
        }

        new_width = (t+(p-m)*w)>0 ? w*m*2 : t + (p+m*w);
        acc_tx.setUpperBound(t+(p+m)*w);
        acc_ty.setUpperBound(t+(p+m)*w);
        acc_tz.setUpperBound(t+(p+m)*w);
        mag_tx.setUpperBound(t+(p+m)*w);
        mag_ty.setUpperBound(t+(p+m)*w);
        mag_tz.setUpperBound(t+(p+m)*w);
        alt_t1.setUpperBound(t+(p+m)*w);
        alt_t2.setUpperBound(t+(p+m)*w);
        volt_t1.setUpperBound(t+(p+m)*w);
        volt_t2.setUpperBound(t+(p+m)*w);
        volt_t3.setUpperBound(t+(p+m)*w);
        volt_t4.setUpperBound(t+(p+m)*w);

        acc_tx.setTickUnit(new_width/6.0);
        acc_ty.setTickUnit(new_width/6.0);
        acc_tz.setTickUnit(new_width/6.0);
        mag_tx.setTickUnit(new_width/6.0);
        mag_ty.setTickUnit(new_width/6.0);
        mag_tz.setTickUnit(new_width/6.0);
        alt_t1.setTickUnit(new_width/6.0);
        alt_t2.setTickUnit(new_width/6.0);
        volt_t1.setTickUnit(new_width/6.0);
        volt_t2.setTickUnit(new_width/6.0);
        volt_t3.setTickUnit(new_width/6.0);
        volt_t4.setTickUnit(new_width/6.0);

        //App.mainLog.debug("zoom by "+2*m+" : w="+new_width + "tick unit = "+new_width/6.0);
    }

    EventHandler<MouseEvent> startDrag = new EventHandler<MouseEvent>(){
        @Override
        public void handle(MouseEvent e){
            orgY = e.getY();
        }
    };

    EventHandler<MouseEvent> dragEvent = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            double y = event.getY();
            double p = (orgY-y);
        }
    };

    private class TimeFormatter extends StringConverter<Number>{
        @Override
        public String toString(Number object){
            int deltaT = 50; // in ms
            int time = object.intValue() * deltaT; // time in ms;
            int time1 = time;
            int sec = (time/1000)%60;
            int ms = time%1000;
            int min = time/60000;
            return String.format("%3d:%02d.%03d",min,sec,ms);
        }

        @Override
        public Number fromString(String string){return null;}
    }
}
