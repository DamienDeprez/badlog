import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Observable;
import java.util.ResourceBundle;

public class App extends Application{

    static DataObserver dataObserver = new DataObserver();
    static SerialComm comm = new SerialComm();
    static int mode = 0;
    static ResourceBundle resources;
    static File save_file = null;
    static Stage primaryStage;
    public static Logger mainLog = LogManager.getLogger(App.class);

    @Override
    public void start(Stage primaryStage) throws Exception{
        App.primaryStage = primaryStage;
        resources = ResourceBundle.getBundle("lang.interface");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("BadLog.fxml"),resources);
        Parent root = loader.load();
        loader.getController();
        primaryStage.setTitle("LELEC2811 - 4 Sensors Logger");

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }

}
