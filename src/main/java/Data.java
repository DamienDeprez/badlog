import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Data implements Serializable, Comparable{

    private int record_number;
    private int accelerometer_x;
    private int accelerometer_y;
    private int accelerometer_z;

    private int magnetometer_x;
    private int magnetometer_y;
    private int magnetometer_z;

    private float pressure;
    private float temperature;

    private float voltage_20;
    private float voltage_21;
    private float voltage_22;
    private float voltage_23;

    private static int number = 0;

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return String.format("║ %15d ║ %7d │ %7d │ %7d ║ %7d │ %7d │ %7d ║ %14.3f ║ %18.3f ║ %8.3f │ %8.3f │ %8.3f │ %8.3f ║\n",
                record_number,
                accelerometer_x,accelerometer_y,accelerometer_z,
                magnetometer_x,magnetometer_y,magnetometer_z,
                pressure,temperature,
                voltage_20,voltage_21,voltage_22, voltage_23);
        /*"  <tr>\n" +
                "    <td>"+record_number+"</td>\n" +
                "    <td>"+accelerometer_x+"</td>\n" +
                "    <td>"+accelerometer_y+"</td>\n" +
                "    <td>"+accelerometer_z+"</td>\n" +
                "    <td>"+magnetometer_x+"</td>\n" +
                "    <td>"+magnetometer_y+"</td>\n" +
                "    <td>"+magnetometer_z+"</td>\n" +
                "    <td>"+temperature+"</td>\n" +
                "    <td>"+pressure+"</td>\n" +
                "    <td>"+voltage_20+"</td>\n" +
                "    <td>"+voltage_21+"</td>\n" +
                "    <td>"+voltage_22+"</td>\n" +
                "    <td>"+voltage_23+"</td>\n" +
                "  </tr>\n";/*"Data{" +
                "record_number=" + record_number +
                ", accelerometer_x=" + accelerometer_x +
                ", accelerometer_y=" + accelerometer_y +
                ", accelerometer_z=" + accelerometer_z +
                ", magnetometer_x=" + magnetometer_x +
                ", magnetometer_y=" + magnetometer_y +
                ", magnetometer_z=" + magnetometer_z +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                ", voltage_20=" + voltage_20 +
                ", voltage_21=" + voltage_21 +
                ", voltage_22=" + voltage_22 +
                ", voltage_23=" + voltage_23 +
                "}\n";*/
    }

    public Data(){
        record_number = number;
        number++;

        accelerometer_x = 0;
        accelerometer_y = 0;
        accelerometer_z = 0;

        magnetometer_x = 0;
        magnetometer_y = 0;
        magnetometer_z = 0;

        pressure = 0.0f;
        temperature = 0.0f;

        voltage_20 = 0.0f;
        voltage_21 = 0.0f;
        voltage_22 = 0.0f;
        voltage_23 = 0.0f;
    }

    public Data(int record_number, int acc_x, int acc_y, int acc_z,
                int mag_x, int mag_y, int mag_z, float pres, float temp,
                float v20, float v21, float v22, float v23){
        this.record_number = record_number;

        this.accelerometer_x = acc_x;
        this.accelerometer_y = acc_y;
        this.accelerometer_z = acc_z;

        this.magnetometer_x = mag_x;
        this.magnetometer_y = mag_y;
        this.magnetometer_z = mag_z;

        this.pressure = pres;
        this.temperature = temp;

        this.voltage_20 = v20;
        this.voltage_21 = v21;
        this.voltage_22 = v22;
        this.voltage_23 = v23;
    }

    public int getRecord_number() {
        return record_number;
    }

    public void setRecord_number(int record_number) {
        this.record_number = record_number;
    }

    public int getAccelerometer_x() {
        return accelerometer_x;
    }

    public void setAccelerometer_x(int accelerometer_x) {
        this.accelerometer_x = accelerometer_x;
    }

    public int getAccelerometer_y() {
        return accelerometer_y;
    }

    public void setAccelerometer_y(int accelerometer_y) {
        this.accelerometer_y = accelerometer_y;
    }

    public int getAccelerometer_z() {
        return accelerometer_z;
    }

    public void setAccelerometer_z(int accelerometer_z) {
        this.accelerometer_z = accelerometer_z;
    }

    public int getMagnetometer_x() {
        return magnetometer_x;
    }

    public void setMagnetometer_x(int magnetometer_x) {
        this.magnetometer_x = magnetometer_x;
    }

    public int getMagnetometer_y() {
        return magnetometer_y;
    }

    public void setMagnetometer_y(int magnetometer_y) {
        this.magnetometer_y = magnetometer_y;
    }

    public int getMagnetometer_z() {
        return magnetometer_z;
    }

    public void setMagnetometer_z(int magnetometer_z) {
        this.magnetometer_z = magnetometer_z;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getVoltage_20() {
        return voltage_20;
    }

    public void setVoltage_20(float voltage_20) {
        this.voltage_20 = voltage_20;
    }

    public float getVoltage_21() {
        return voltage_21;
    }

    public void setVoltage_21(float voltage_21) {
        this.voltage_21 = voltage_21;
    }

    public float getVoltage_22() {
        return voltage_22;
    }

    public void setVoltage_22(float voltage_22) {
        this.voltage_22 = voltage_22;
    }

    public float getVoltage_23() {
        return voltage_23;
    }

    public void setVoltage_23(float voltage_23) {
        this.voltage_23 = voltage_23;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Data) {
            Data d = (Data) o;
            return Integer.compare(record_number,d.getRecord_number());
        }
        return 0;
    }
}
