import com.fazecast.jSerialComm.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by damien on 7/11/17.
 */
public class SerialComm implements Runnable {

        private SerialPort serialPort;
        private SerialPort [] serialPorts;
        private int baudRate = 115200;
        private boolean reading = false;

        public SerialComm(){
            serialPorts = SerialPort.getCommPorts();
        }

        public void setBaudRate(int baudRate){
            this.baudRate = baudRate;
    }

        public String[] getPorts(){
            String portName [] = new String[serialPorts.length];
            for(int i = 0; i<serialPorts.length; i++){
                portName[i] = serialPorts[i].getSystemPortName();
            }
            return portName;
        }

        public void closePort(){
            App.mainLog.info("Close port : "+serialPort.getSystemPortName());
            serialPort.closePort();
        }

        public void openPort(String portName){
            serialPort = SerialPort.getCommPort(portName);
            serialPort.setBaudRate(this.baudRate);
            App.mainLog.info("open port : "+serialPort.getSystemPortName() + "@ "+this.baudRate+" Bd");
            serialPort.openPort();
        }

        public void readPort(){
            Thread t = new Thread(this);
            t.start();
        }

        public void writePort(String msg){
            App.mainLog.debug("Write \""+msg+"\" in the port : "+serialPort.getSystemPortName() );
            byte [] buffer = msg.getBytes();
            serialPort.writeBytes(buffer, buffer.length);
        }

        public boolean isRunning(){
            return reading;
        }

    @Override
    public void run() {
        App.mainLog.debug("Start reading port : "+serialPort.getSystemPortName());
        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
        reading = true;
        boolean end = false;
        StringBuilder builder = new StringBuilder();
        while(!end){
            byte [] readBuffer = new byte[64];
            int readNum = serialPort.readBytes(readBuffer, readBuffer.length);
            String data = new String(readBuffer,0,readNum);
            end = data.contains("$");
            builder.append(data);
            String buffer = builder.toString();
            if(buffer.contains("S0") && buffer.contains("0S")){
                int b = buffer.indexOf("S0");
                int e = buffer.indexOf("0S");
                if(b>e) {
                    builder.delete(0, b);
                    App.mainLog.debug("Remove the begin of the buffer");
                }
                else{
                    String [] split = buffer.substring(b,e+2).split(" ");
                    int rn = Integer.parseInt(split[1]); // record number
                    int ax = Integer.parseInt(split[2]); // record number
                    int ay = Integer.parseInt(split[3]); // record number
                    int az = Integer.parseInt(split[4]); // record number
                    float p = Float.parseFloat(split[5]);
                    float t = Float.parseFloat(split[6]);
                    int mx = Integer.parseInt(split[7]);
                    int my = Integer.parseInt(split[8]);
                    int mz = Integer.parseInt(split[9]);
                    float v1 = Float.parseFloat(split[10]);
                    float v2 = Float.parseFloat(split[11]);
                    float v3 = Float.parseFloat(split[12]);
                    float v4 = Float.parseFloat(split[13]);

                    Data data1 = new Data(rn,ax,ay,az,mx,my,mz,p,t,v1,v2,v3,v4);
                    App.dataObserver.add(data1);
                    builder.delete(b,e+2);
                }
            }
        }
        reading = false;
        closePort();
    }
}
